class CreateStores < ActiveRecord::Migration[5.2]
  def change
    create_table :stores do |t|
      t.string :name, null: false
      t.text :address, null: false
      t.string :email, default: 'francisco.abalan@pjchile.com'
      t.string :phone

      t.timestamps
    end
  end
end
