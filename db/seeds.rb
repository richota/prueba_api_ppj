# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Order.destroy_all
Store.destroy_all
Product.destroy_all

Store.create(name: "Macul", address: "Av Pedro Alessandri 4588, Macul", phone: "+56944558877")
Store.create(name: "Tobalaba", address: "Tobalaba 500, Providencia", email: "rsuarez.adv@gmail.com", phone: "+56947901919")

Product.create(name: "Hawaiana Familiar Delgada", sku: "14HWTHN", category:"pizza", price: 12000)
Product.create(name: "Pepperoni Doble Familiar", sku: "14PPD", category:"pizza", price: 14000)
Product.create(name: "Italiana Borde Queso Mediana", sku: "12ITABQ", category:"pizza", price: 13600)
Product.create(name: "Pollo BBQ Mediana", sku: "12BBQHT", category:"pizza", price: 1200)
Product.create(name: "Vegetariana Extra Grande", sku:"16VEGGI", category:"pizza", price: 16600)
Product.create(name: "Palos de Ajo", sku:"PAJO", category: "acompañamiento", price:3500)
Product.create(name: "Alitas de Pollo", sku:"8WINGS", category: "acompañamiento", price: 4400)
Product.create(name: "Coca-Cola 2.5L", sku: "25CC", category: "bebida", price: 3000)
Product.create(name: "Sprite Zero 1.5L", sku:"15SPZ", category:"bebida", price: 2200)
Product.create(name: "Brownie", sku: "CBR9", category: "postre", price: 5700)
Product.create(name: "Waffles", sku: "2WFS", category: "postre", price: 2900)

Store.all.each do |store|
  Product.all.each do |product|
    StoreProduct.create(store_id: store.id, product_id: product.id)
  end
end