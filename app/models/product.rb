class Product < ApplicationRecord
  validates :sku, uniqueness:true, presence: true
  has_many :store_products, dependent: :destroy
  has_many :stores, through: :store_products
  has_many :order_products, dependent: :destroy
end
