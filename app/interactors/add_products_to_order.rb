class AddProductsToOrder
  include Interactor

  delegate :order_params, :order_products_params, to: :context

  before do
    context.order_products = []
    context.errors = []
  end

  def call
    iterate_products(order_params, order_products_params)
  end

  def iterate_products(order, order_products)
    order_products.each do |p|
      product = Product.find_by_sku(p[:sku])
      order.order_products.create(product_id: product.id, order_id: order.id, quantity: p[:quantity]) if product
    end
    context.order_products = order.order_products
  end
end