json.order @order
json.order_products @order.order_products do |op|
  json.id op.id
  json.product_name op.product.name
  json.product_price op.product.price
  json.quantity op.quantity
end