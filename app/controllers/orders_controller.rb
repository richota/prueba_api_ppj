class OrdersController < ApplicationController
  # Ex body
  # {
  #   "order":{
  #     "store":1,
  #     "products":[
  #       {"sku":"14PPD","quantity":2},
  #       {"sku":"PAJO","quantity":2},
  #       {"sku":"25CC","quantity":1}
  #       ]
  #   }
  # }
  def create
    @order = Order.new
    @order.store_id = params[:order][:store]
    @order.save
    AddProductsToOrder.call(order_params: @order, order_products_params: params[:order][:products])
    total = OrderTotal.new(@order)
    @order.update(total: total.total_amount)
    # render json: @order, status: :created, location: @order
  end

  def index
    @orders = Order.all
    render json: @orders
  end

  def show
    @order = Order.find(params[:id])
    render json: @order
  end
end
